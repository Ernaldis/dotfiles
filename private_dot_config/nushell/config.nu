$env.config.buffer_editor = "hx"
$env.VISUAL = "hx"
$env.EDITOR = $env.VISUAL
$env.MCFLY_KEY_SCHEME = "vim"
$env.config.show_banner = false
$env.STARSHIP_SHELL = "nu"
$env.BARTIB_FILE = "/home/timothy/.config/activities.bartib"
# $env.NAVI_CONFIG = ".local/share/navi/cheats/"

source ~/.config/nushell/integrations.nu
source ~/.config/nushell/aliases.nu
source ~/.config/nushell/functions.nu
